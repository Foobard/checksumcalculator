#  This is the PyQt5 checksum calculator that can generate and compare
#  the md5, sha1, sha224, sha256, sha384 and sha512 checksums
#  for any file
#
#  Copyright (c) 2016 Riverbank Computing Limited <info@riverbankcomputing.com>
#
#  This file is part of CheckSumCalculator
#
#  This file may be used under the terms of the GNU General Public License
#  version 3.0 as published by the Free Software Foundation and appearing in
#  the file LICENSE included in the packaging of this file.  Please review the
#  following information to ensure the GNU General Public License version 3.0
#  requirements will be met: http://www.gnu.org/copyleft/gpl.html.
#
#  If you do not wish to use this file under the terms of the GPL version 3.0
#  then you may purchase a commercial license.  For more information contact
#  info@riverbankcomputing.com.
#
#  This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
#  WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

from contextlib import contextmanager

from PyQt5.QtCore import QFile, QTextStream, QDir, Qt
from PyQt5.QtGui import QIcon, QPixmap, QCursor
from PyQt5.QtWidgets import (QApplication, QHBoxLayout, QVBoxLayout,
                             QFrame, QLineEdit, QPushButton,
                             QRadioButton, QGroupBox, QMainWindow,
                             QAction, QFileDialog, QMessageBox, QToolBar,
                             QLabel)


class MainWindow(QMainWindow):
    x = 300
    y = 300
    width = 500
    height = 200

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setGeometry(MainWindow.x, MainWindow.y,
                         MainWindow.width, MainWindow.height)
        self.setWindowTitle('CheckSum Calculator')
        self.frame = CentralFrame()
        self.setCentralWidget(self.frame)


class Button:
    def createToolBarButton(self, iconPicPath, description, slot):
        action = QAction(QIcon(iconPicPath), description, self)
        action.triggered.connect(slot)
        button = QToolBar()
        button.addAction(action)
        return button

    def createRadioButton(self, text, buttonStatus=False):
        button = QRadioButton(text)
        button.setChecked(buttonStatus)
        return button

    def createPushButton(self, text, slot):
        button = QPushButton(text)
        button.clicked.connect(slot)
        return button


class CentralFrame(QFrame, Button):
    stylesheet = './styles/styles.qss'
    pics = './styles/pics/'

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setFrameStyle(QFrame.Panel | QFrame.Raised)
        self.setLineWidth(2)

        self.browseButton = self.createToolBarButton(CentralFrame.pics +
                                                     'chooseFileBtn.png',
                                                     'choose file',
                                                     self.browse)

        self.calculateButton = self.createToolBarButton(CentralFrame.pics +
                                                        'calculateBtn.png',
                                                        'calculate',
                                                        self.calculate)

        self.compareButton = self.createPushButton('Compare',
                                                   self.compareSums)

        self.md5Button = self.createRadioButton('md5')
        self.sha1Button = self.createRadioButton('sha1', True)
        self.sha224Button = self.createRadioButton('sha224')
        self.sha256Button = self.createRadioButton('sha256')
        self.sha384Button = self.createRadioButton('sha384')
        self.sha512Button = self.createRadioButton('sha512')

        self.fileLine = QLineEdit()
        self.checkSumLine = QLineEdit()
        self.originalCheckSumLine = QLineEdit()

        fileBox = self.createStyledGroupBox('File:')
        hashFunctionBox = self.createStyledGroupBox('Hash function:')
        checkSumBox = self.createStyledGroupBox('Checksum:')
        originalCheckSumBox = self.createStyledGroupBox('Original checksum:')

        fileBoxLayout = QHBoxLayout()
        fileBoxLayout.addWidget(self.fileLine)
        fileBoxLayout.addWidget(self.browseButton)
        fileBox.setLayout(fileBoxLayout)

        hashFunctionBoxLayout = QHBoxLayout()
        hashFunctionBoxLayout.addWidget(self.md5Button)
        hashFunctionBoxLayout.addWidget(self.sha1Button)
        hashFunctionBoxLayout.addWidget(self.sha224Button)
        hashFunctionBoxLayout.addWidget(self.sha256Button)
        hashFunctionBoxLayout.addWidget(self.sha384Button)
        hashFunctionBoxLayout.addWidget(self.sha512Button, 1)
        hashFunctionBoxLayout.addWidget(self.calculateButton)
        hashFunctionBox.setLayout(hashFunctionBoxLayout)

        checkSumBoxLayout = QHBoxLayout()
        checkSumBoxLayout.addWidget(self.checkSumLine)
        checkSumBox.setLayout(checkSumBoxLayout)

        originalCheckSumBoxLayout = QHBoxLayout()
        originalCheckSumBoxLayout.addWidget(self.originalCheckSumLine)
        originalCheckSumBox.setLayout(originalCheckSumBoxLayout)

        footerLogo = QPixmap(CentralFrame.pics + 'logo.png')
        footerLabel = QLabel()
        footerLabel.setPixmap(footerLogo)
        footerLayout = QHBoxLayout()
        footerLayout.addWidget(footerLabel, 1)
        footerLayout.addWidget(self.compareButton)

        mainLayout = QVBoxLayout()

        mainLayout.addWidget(fileBox, 1)
        mainLayout.addWidget(self.underline())
        mainLayout.addWidget(hashFunctionBox, 1)
        mainLayout.addWidget(self.underline())
        mainLayout.addWidget(checkSumBox, 1)
        mainLayout.addWidget(self.underline())
        mainLayout.addWidget(originalCheckSumBox, 1)
        mainLayout.addWidget(self.underline())
        mainLayout.addLayout(footerLayout)

        self.setLayout(mainLayout)

    def underline(self):
        separator = QFrame()
        separator.setFrameStyle(QFrame.HLine | QFrame.Sunken)
        separator.setLineWidth(2)
        return separator

    def browse(self):
        fpath, _ = QFileDialog.getOpenFileName(self,
                                               'choose file',
                                               QDir.currentPath())
        if fpath:
            self.fileLine.setText(fpath)

    def calculate(self):
        if self.fileLine.text():
            with open(self.fileLine.text(), 'rb') as f:
                radioButtons = [self.md5Button,
                                self.sha1Button,
                                self.sha224Button,
                                self.sha256Button,
                                self.sha384Button,
                                self.sha512Button]

                for radioButton in radioButtons:
                    if radioButton.isChecked():
                        checkSum = getattr(hashlib, radioButton.text())()
                        with self.waitCursor():
                            for chunk in iter(lambda: f.read(4096), b""):
                                checkSum.update(chunk)
                self.checkSumLine.setText(checkSum.hexdigest())
        else:
            self.showMessageBox('...', 'File field is empty', 'w')

    def getStyleSheet(self, qss_path):
        f = QFile(qss_path)
        f.open(QFile.ReadOnly | QFile.Text)
        stylesheet = QTextStream(f).readAll()
        f.close()
        return stylesheet

    def createStyledGroupBox(self, title):
        groupBox = QGroupBox()
        groupBox.setTitle(title)
        groupBox.setStyleSheet(self.getStyleSheet(CentralFrame.stylesheet))
        return groupBox

    def showMessageBox(self, title, info, messageType):
        if messageType == 'w':  # w - warning
            msgBox = QMessageBox.warning(self, title, info)
        elif messageType == 'n':  # n - notification
            msgBox = QMessageBox.information(self, title, info)
        return msgBox

    def compareSums(self):
        if self.checkSumLine.text():
            if self.originalCheckSumLine.text():
                if self.checkSumLine.text() == self.originalCheckSumLine.text():
                    self.showMessageBox('...',
                                        'matched',
                                        'n')
                else:
                    self.showMessageBox('...',
                                        'not matched',
                                        'n')
            else:
                self.showMessageBox('...',
                                    'original checksum field is empty',
                                    'w')
        else:
            self.showMessageBox('...',
                                'checksum field is empty',
                                'w')

    @contextmanager
    def waitCursor(self):
        try:
            QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
            yield
        finally:
            QApplication.restoreOverrideCursor()

if __name__ == "__main__":

    import sys
    import hashlib

    app = QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
